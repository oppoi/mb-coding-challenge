# coding-challenge

## Getting started

- npm install
- npm run test

## Test

Use jest to test the given function

- npm run test

***

## Wie ist die Laufzeit Ihres Programms?

Die Laufzeit ist sehr stark von der Anzahl der Intervalle abhängig. Je höher die Anzahl der Intervalle, desto mehr Rechenleistung wird benötigt. Die Komplexität beträgt O(n) und erfolgt linear.

## Wie kann die Robustheit sichergestellt werden, vor allem auch mit Hinblick auf sehr große Eingaben?

- Vor dem Ablauf auf Fehler prüfen / Validierung durchführen
- Einsatz von effizienten Datenstrukturen
- Unnötige Wiederholungen (z.B. Loops) vermeiden
- Intervalle schrittweise auslesen 
  - Sofern möglich sollte die komplette Liste an Intervallen nicht aufeinmal ausgelesen bzw. ausgeführt werden

## Wie verhält sich der Speicherverbrauch ihres Programms?

- Steigt stark mit jedem weiteren Interval
- Kann verringert werden, indem man nur so wenig Daten wie möglich lädt
- oder nur die erforderlichen Daten lädt
