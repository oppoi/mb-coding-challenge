import { merge } from './index';
import Interval from "./interfaces/interval"

describe('merge', () => {
  test('merges overlapping intervals', () => {
    const intervals: Interval[] = [[25, 30], [2, 19], [14, 23], [4, 8]];
    const result = merge(intervals);
    const expected: Interval[] = [[2, 23], [25, 30]];
    expect(result).toEqual(expected);
  });
});