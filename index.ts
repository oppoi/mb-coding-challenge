
import Interval from "./interfaces/interval"

/**
 * A function to merge overlapping intervals
 * We assume that his belongs to a class
 * else I would give the function a more proper name
 * @function merge
 * @param intervals Interval[]
 * @returns Interval[]
 */
export const merge = (intervals: Interval[]): Interval[] => {
  // sort by lowest start value
  intervals.sort((a, b) => a[0] - b[0]);

  const mergedIntervals: Interval[] = [];
  let [start, end]: Interval = intervals[0];

  for (let i = 1; i < intervals.length; i++) {
    const [currentStart, currentEnd]: Interval = intervals[i];
    const interval: Interval = intervals[i];

    if (interval[0] <= end) {
      end = Math.max(end, currentEnd);
    } else {
      // interval does not match
      mergedIntervals.push([start, end]);
      [start, end] = [currentStart, currentEnd];
    }
  }

  mergedIntervals.push([start, end]);

  return mergedIntervals;
}